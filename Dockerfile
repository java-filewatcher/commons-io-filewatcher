FROM openjdk:17-slim

WORKDIR /app

ARG  JAR_FILE=target/*.jar
COPY ${JAR_FILE} application.jar

ENTRYPOINT ["java", "-jar", "application.jar"]

package org.gitlab.zartc.commonsiofilewatcher;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.filefilter.FileFilterUtils;
import org.apache.commons.io.monitor.FileAlterationListenerAdaptor;
import org.apache.commons.lang3.time.DurationFormatUtils;
import org.apache.commons.lang3.time.StopWatch;

import picocli.CommandLine;
import picocli.CommandLine.Command;
import picocli.CommandLine.Parameters;

import lombok.extern.log4j.Log4j2;


@Log4j2
@Command(name = "watch", mixinStandardHelpOptions = true, version = "1.0")
public class Application implements Runnable {

    @Parameters(description = "Any number of directory path to watch")
    private final List<String> dirToWatch = new ArrayList<>();


    public void run() {
        try {
            StopWatch stopWatch = StopWatch.createStarted();

            // the filter for the FileWatcher
            var filter = FileFilterUtils.makeFileOnly(FileFilterUtils.suffixFileFilter(".csv"));

            // create the FileWatcher
            var fileWatcher = new FileWatcher();

            // set it up with the directories to watch
            for (String s : dirToWatch) {
                fileWatcher.observe(new File(s), filter, new Listener());
            }

            // start the FileWatcher
            fileWatcher.start();

            // log the time it took to initialize
            log.info("Started in ............. : {}", DurationFormatUtils.formatDuration(stopWatch.getTime(), "ss.SSS 'seconds'"));
        } catch (Exception e) {
            log.catching(e);
        }
    }

    // --------------------------------------------------------------------------------------------


    /**
     * The callbacks that are called when the FileWatcher detect changes
     */
    private static class Listener extends FileAlterationListenerAdaptor {

        @Override
        public void onFileCreate(File file) {
            log.info("onFileCreate() called with: file = [" + file + "]");
        }

        @Override
        public void onFileChange(File file) {
            log.info("onFileChange() called with: file = [" + file + "]");
        }

        @Override
        public void onFileDelete(File file) {
            log.info("onFileDelete() called with: file = [" + file + "]");
        }
    }


    public static void main(String[] args) {
        new CommandLine(new Application()).execute(args);
    }
}

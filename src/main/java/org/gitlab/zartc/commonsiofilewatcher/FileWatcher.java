package org.gitlab.zartc.commonsiofilewatcher;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.filefilter.IOFileFilter;
import org.apache.commons.io.monitor.FileAlterationListener;
import org.apache.commons.io.monitor.FileAlterationObserver;

import lombok.NonNull;
import lombok.extern.log4j.Log4j2;


/**
 * Periodically scan observed directories looking for new/modified/deleted files.
 * When a change is detected, the registered listener is called.
 */
@Log4j2
public class FileWatcher implements Runnable {

    private final ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();

    private final List<FileAlterationObserver> observers = new ArrayList<>();

    private final long scanInterval;

    /**
     * the task that is woken up periodically to run the registered observers.
     */
    private ScheduledFuture<?> task;


    public FileWatcher() {
        this(200L);
    }

    public FileWatcher(long scanInterval) {
        this.scanInterval = scanInterval;
    }


    public long getScanInterval() {
        return scanInterval;
    }

    /**
     * Register a new observer with the FileWatcher.
     * Multiple directory/filename pattern can be observed by registering multiple observers.
     *
     * @param dir the directory to observe
     * @param filter the filter
     * @param listener the listener that will be invoked whenever changes are detected
     */
    public void observe(@NonNull File dir, @NonNull IOFileFilter filter, @NonNull FileAlterationListener listener) {
        if (!dir.exists() || !dir.isDirectory()) {
            throw new Error("path '%s' does not exists or is not a directory".formatted(dir.getAbsolutePath()));
        }

        // create a new observer for that directory
        var observer = new FileAlterationObserver(dir, filter);

        // attach the client listener to the observer
        observer.addListener(listener);

        // register the observer so that we are able to close it later
        observers.add(observer);
    }

    /**
     * Start the FileWatcher.
     */
    public synchronized void start() throws Exception {
        if (task != null) {
            return;
        }

        for (FileAlterationObserver observer : observers) {
            observer.initialize();
        }

        task = executor.scheduleAtFixedRate(this, 0, getScanInterval(), TimeUnit.MILLISECONDS);

        // install a shutdown hook
        Runtime.getRuntime().addShutdownHook(new Thread(this::stop));

        // print the list of observers that were started
        if (log.isInfoEnabled()) {
            observers.forEach(log::info);
        }
    }

    /**
     * Stop the FileWatcher.
     */
    public synchronized void stop() {
        if (task == null) {
            return;
        }

        task.cancel(false);

        for (FileAlterationObserver observer : observers) {
            try {
                observer.destroy();
            } catch (Throwable e) {
                log.catching(e);
            }
        }
    }

    @Override
    public void run() {
        for (FileAlterationObserver observer : observers) {
            try {
                observer.checkAndNotify();
            } catch (Throwable e) {
                // log and continue with next observer
                log.error("{} failed: {}", observer, e.getMessage());
            }
        }
    }
}
